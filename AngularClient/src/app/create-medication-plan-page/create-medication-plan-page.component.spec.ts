import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMedicationPlanPageComponent } from './create-medication-plan-page.component';

describe('CreateMedicationPlanPageComponent', () => {
  let component: CreateMedicationPlanPageComponent;
  let fixture: ComponentFixture<CreateMedicationPlanPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMedicationPlanPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMedicationPlanPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
