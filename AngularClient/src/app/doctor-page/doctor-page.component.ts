import { Component, OnInit } from '@angular/core';
import {User} from "../model/User";
import {UserService} from "../service/user-service";
import {Router} from "@angular/router";
import {Patient} from "../model/Patient";
import {Caregiver} from "../model/Caregiver";
import {HttpHeaders} from "@angular/common/http";


@Component({
  selector: 'app-user-list',
  templateUrl: './doctor-page.component.html',
  styleUrls: ['./doctor-page.component.css'],
})
export class DoctorPageComponent{
  patients: Patient[];
  caregivers:Caregiver[];
  selectedTypeOfUser:string;
  headers:HttpHeaders;
  constructor(private userService:UserService, private router:Router) {
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","doctor");
  }

  displayUsers(selectedTypeOfUsers:string) {
    this.userService.findPatients(this.headers).subscribe(data=>{
      this.patients=data;
    });
    this.userService.findCaregivers(this.headers).subscribe(data => {
      this.caregivers = data;
    });

    this.selectedTypeOfUser = selectedTypeOfUsers;
  }

  createUserPage(){
    this.router.navigate(["/doctorPage/createUser"])
  }

  createMedicationPage(){
    this.router.navigate(["/doctorPage/createMedicationPlan"])
  }

  deleteUser(id:number){
    let userResult=[];
    userResult.push(...this.patients)
    userResult.push(...this.caregivers)
    for(let user of userResult)
      if(user.id==id) {
        console.log(user);
        if (user.userROLE == 'Caregiver') {
          this.userService.deleteUser(user, this.selectedTypeOfUser,this.headers).subscribe().add(() => {
            this.userService.uncarePatients(user.patients,this.headers).subscribe();
          });
        } else {
          this.userService.deleteUser(user, this.selectedTypeOfUser,this.headers).subscribe()
        }
      }
  }

  modifyUser(user:any){
      localStorage.setItem("user_modify",JSON.stringify(user));
      this.router.navigate(["/doctorPage/modifyUser"]);
  }

}
