import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from "@angular/router";
import {Caregiver} from "../model/Caregiver";
import {Patient} from "../model/Patient";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{
  canActivate(next: ActivatedRouteSnapshot): boolean{
    let userLogged=JSON.parse(localStorage.getItem("user_guard"));
    let RegexDoc=/doctorPage\/*[a-zA-Z\/]*/;
    let RegexPatient=/patientPage/;
    let RegexCaregiver=/caregiverPage/;

    console.log(userLogged);

    if(userLogged==null){
      this.router.navigate(['']);
      return false;
    }

    console.log(next.routeConfig.path);
    console.log(RegexDoc.test(next.routeConfig.path));

    if (userLogged.userROLE === 'Doctor' && RegexDoc.test(next.routeConfig.path))
        return true;

    if (userLogged.userROLE === 'Patient' && RegexPatient.test(next.routeConfig.path))
        return true;

    if (userLogged.userROLE === 'Caregiver' && RegexCaregiver.test(next.routeConfig.path))
        return true;

      this.router.navigate(['']);
      return false;
  }


  constructor(private router:Router) { }
}
