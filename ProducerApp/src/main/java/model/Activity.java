package model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;

public class Activity {
    private String name;
    private Long patientID;
    private LocalDateTime start_time;
    private LocalDateTime end_time;

    public Activity(String name, Long patientID, LocalDateTime start_time, LocalDateTime end_time) {
        this.name = name;
        this.patientID = patientID;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public Activity(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPatientID() {
        return patientID;
    }

    public void setPatientID(Long patientID) {
        this.patientID = patientID;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public LocalDateTime getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalDateTime end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        return "model.Activity{" +
                "name='" + name + '\'' +
                ", patientID=" + patientID +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                '}';
    }
}
