package model;

public class Credential {
    private String name;
    private String pass;

    public Credential(String name, String pass) {
        this.name = name;
        this.pass = pass;
    }

    public String getUsername() {
        return name;
    }

    public void setUsername(String name) {
        this.name = name;
    }

    public String getPassword() {
        return pass;
    }

    public void setPassword(String pass) {
        this.pass = pass;
    }
}
