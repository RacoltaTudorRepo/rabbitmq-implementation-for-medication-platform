package security;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CaregiverFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException

    {

        HttpServletRequest req = (HttpServletRequest) request;
        String role=req.getHeader("role");

            if(req.getMethod().equals("OPTIONS")){
                chain.doFilter(request, response);
                return;
            }

        if(role!=null && !role.equals("caregiver")){
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is not valid.");
        }
        else if(role==null){
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is not valid.");
        }

        else{
            chain.doFilter(request, response);
        }
    }
}
