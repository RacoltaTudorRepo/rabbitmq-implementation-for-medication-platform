package angularspring.project.test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import interfacing.sensor.Receiver;
import model.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import repository.*;
import security.*;

@EnableJpaRepositories(basePackages = "repository")
@EntityScan(basePackages = {"model","interfacing"})
@ComponentScan({"rest_services","service","interfacing","notifications"})
@EnableWebSocketMessageBroker
@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);

	}

	@Bean
	public FilterRegistrationBean<LoggingFilter> loggingFilter(){
		FilterRegistrationBean<LoggingFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new LoggingFilter());
		registrationBean.addUrlPatterns("/backend/*");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<PatientFilter> patientFilter(){
		FilterRegistrationBean<PatientFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new PatientFilter());
		registrationBean.addUrlPatterns("/backend/patient/*");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<DoctorFilter> doctorFilter(){
		FilterRegistrationBean<DoctorFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new DoctorFilter());
		registrationBean.addUrlPatterns("/backend/doctor/*");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<MedicationFilter> medicationFilter(){
		FilterRegistrationBean<MedicationFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new MedicationFilter());
		registrationBean.addUrlPatterns("/backend/medication/*");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<CaregiverFilter> caregiverFilter(){
		FilterRegistrationBean<CaregiverFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new CaregiverFilter());
		registrationBean.addUrlPatterns("/backend/caregiver/*");

		return registrationBean;
	}

	@Bean
	CommandLineRunner init(Receiver receiver){
		return args->{
			receiver.connectAsReceiver();
		};
	}
	
//	@Bean
//	CommandLineRunner init(UserRepository userRepository, MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository) {
//		return args -> {
//			Doctor doctor = new Doctor("doctor1","blabla","George",new Date(1960,10,26),"M",null);
//			Patient patient1=new Patient("patient1","blabla","Radu",null,null,null,"Amigdalita");
//			patient1.setCared(true);
//			Patient patient2=new Patient("patient2","blabla","Viorel",null,null,null,"Gastrita,Ulcer");
//			Caregiver caregiver=new Caregiver("caregiver1","blabla","Tudor",null,null,null,Arrays.asList(patient1));
//
//
//			userRepository.save(doctor); userRepository.save(patient1); userRepository.save(patient2);
//			userRepository.save(caregiver);
//
//
//			Medication medication=new Medication("Sanax",Arrays.asList("nush","nush1"),300);
//			medication.setInPlan(true);
//			MedicationPlan medicationPlan=new MedicationPlan(Long.parseLong("9"),Arrays.asList(medication),null,300);
//			MedicationPlan medicationPlan=new MedicationPlan(Long.parseLong("3"),Arrays.asList(medication),null,300);
//
//			medicationRepository.save(medication);
//			medicationPlanRepository.save(medicationPlan);
//        };
//	}

}
