package repository;

import model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Long> {
    List<MedicationPlan> findAllByPatientID(Long id);
}
