package repository;

import com.sun.xml.internal.bind.v2.model.core.ID;
import interfacing.sensor.Activity;
import org.springframework.data.repository.CrudRepository;

public interface ActivityRepository extends CrudRepository<Activity, ID> {
}
